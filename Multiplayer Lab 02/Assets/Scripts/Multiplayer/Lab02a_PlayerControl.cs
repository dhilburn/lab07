﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Lab02a_PlayerControl : NetworkBehaviour 
{
	struct PlayerState
    {
        public float posX, posY, posZ;
        public float rotX, rotY, rotZ;
    }

    [SyncVar]
    PlayerState state;

	// Use this for initialization
	void Start ()
    {
        InitState();
        SyncState();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (isLocalPlayer)
        {
            KeyCode[] possibleKeys = { KeyCode.A, KeyCode.S, KeyCode.D, KeyCode.W, KeyCode.Q, KeyCode.E };
            foreach (KeyCode possibleKey in possibleKeys)
            {
                if (!Input.GetKey(possibleKey))
                    continue;
                CmdMoveOnServer(possibleKey);
            }
        }
        SyncState();
	}

    void InitState()
    {
        state = new PlayerState
        {
            posX = -119f,
            posY = 165.08f,
            posZ = -924f,
            rotX = 0,
            rotY = 0,
            rotZ = 0
        };
    }

    void SyncState()
    {
        transform.position = new Vector3(state.posX, state.posY, state.posZ);
        transform.rotation = Quaternion.Euler(state.rotX, state.rotY, state.rotZ);
    }

    PlayerState Move(PlayerState previous, KeyCode newKey)
    {
        float dx = 0, dy = 0, dz = 0;
        float dRotY = 0;

        switch(newKey)
        {
            case (KeyCode.Q):
                dRotY = -1f;
                break;
            case (KeyCode.S):
                dz = -0.5f;
                break;
            case (KeyCode.E):
                dRotY = 1f;
                break;
            case (KeyCode.W):
                dz = 0.5f;
                break;
            case (KeyCode.A):
                dx = -0.5f;
                break;
            case (KeyCode.D):
                dx = 0.5f;
                break;
        }

        return new PlayerState
        {
            posX = dx + previous.posX,
            posY = dy + previous.posY,
            posZ = dz + previous.posZ,
            rotX = previous.rotX,
            rotY = dRotY + previous.rotY,
            rotZ = previous.rotZ
        };
    }

    [Command]
    void CmdMoveOnServer(KeyCode pressedKey)
    {
        state = Move(state, pressedKey);
    }
}
