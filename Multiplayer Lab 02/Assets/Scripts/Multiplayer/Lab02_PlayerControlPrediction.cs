﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

// Enum for determining which animation state to show.
public enum CharacterState
{
    IDLE = 0,
    WALK_FORWARD = 1,
    WALK_BACKWARD = 2,
    RUN_FORWARD = 3,
    RUN_BACKWARD = 4,
    JUMP = 5
}

/**
    @authors Darrick Hilburn, Tiffany Fisher

    This script shows a player in a server using Player Prediction.
    */
public class Lab02_PlayerControlPrediction : NetworkBehaviour 
{
    #region Fields

    // Player's state in the server
    [SyncVar(hook = "OnServerStateChanged")]
    PlayerState serverState;
    // Player's state on their own end
    PlayerState predictedState;
    // Queue of player movements to make
    Queue<KeyCode> pendingMoves;
    // CharacterState for determining which animation to play
    CharacterState characterAnimationState;
    // Flag for if the character is jumping
    bool running = false;
    // Flag for if the character is running
    bool jumping = false;
    // Reference to the character rigidbody; unused for this implementation
    Rigidbody unityGuyRB;
    // List of keys that affect the player character
    KeyCode[] possibleKeys = { KeyCode.A, KeyCode.S, KeyCode.D, KeyCode.W, KeyCode.Q, KeyCode.E, KeyCode.LeftShift, KeyCode.Space };

    [Tooltip("Player walk speed.")]
    public float moveSpeed = 0.5f;
    [Tooltip("Player run speed.")]
    public float runSpeed = 1f;
    [Tooltip("Player rotation speed.")]
    public float rotateSpeed = 1f;
    [Tooltip("Force that makes the player jump.")]
    public float jumpHeight;
    [Tooltip("Animator within the player.")]
    public Animator controller;

    // Structure for holding a player's position, number of keys pressed, 
    //    and animation state.
    struct PlayerState
    {
        public int movementNumber;
        public float posX, posY, posZ;
        public float rotX, rotY, rotZ;
        public CharacterState animationState;
    };
    #endregion


    #region Methods

    /**
        @authors Darrick Hilburn, Tiffany Fisher

        Start initializes the player and server states, and initializes the key presses queue
           for the local player.
    */
    void Start()
    {
        unityGuyRB = GetComponent<Rigidbody>();
        if(unityGuyRB.Equals(null))
        {
            Debug.Log("Unity guy don't need no physics holding him down!");
        }
        else
        {
            Debug.Log("Unity guy is under the control of physics!");
        }
        InitState();
        predictedState = serverState;
        if (isLocalPlayer)
        {
            pendingMoves = new Queue<KeyCode>();
            UpdatePredictedState();
        }
        SyncState();
    }

    /**
        @authors Darrick Hilburn, Tiffany Fisher
        
        Update checks the input the player has made compared to a list of keycodes.
            If the player pressed a key that is in the keycodes list,
            The key is added to the queue of inputs.
            Regardless of input,
            The predicted state of the client is updated and the server is sent a move command.   
            The player is then syncronized to reflect their current state.
     */
    void Update()
    {
        if (isLocalPlayer)
        {
            bool keyPressed = false;
            // Check if the player presses left shift to run or space to jump
            if (Input.GetKeyDown(KeyCode.LeftShift))
            {
                running = !running;
            }
            if (Input.GetKeyDown(KeyCode.Space) && !jumping)
            {
                StartCoroutine(Jump(serverState));
            }
            // Compare a caught keypress to the list of initialized possible keys
            foreach (KeyCode possibleKey in possibleKeys)
            {
                // If the keypress does not match a possible key, do nothing
                if (!Input.GetKey(possibleKey))
                    continue;
                // If the keypress does match a possible key, set the flag to true,
                //    enqueue the keypress into pending moves, update the predicted state on
                //    the client, and tell the server to move
                keyPressed = true;
                //Debug.Log("Adding to pending moves.");
                pendingMoves.Enqueue(possibleKey);
                //Debug.Log("Pending moves: " + pendingMoves.Count);
                UpdatePredictedState();
                CmdMoveOnServer(possibleKey);
            }
            if (!keyPressed)
            {
                // enqueue a null key into possible moves, update the predicted client state,
                //    and tell the server to move based on the null key if the player did not
                //    press a possible movement key.
                pendingMoves.Enqueue(KeyCode.Alpha0);
                UpdatePredictedState();
                CmdMoveOnServer(KeyCode.Alpha0);
            }            
        }
        SyncState();
    }

    /**
        @author Darrick Hilburn

        FixedUpdate is used for physics.
        
    */
    void FixedUpdate()
    {
        
    }

    /**
        @author Darrick Hilburn

        DebugObjectToServer compares the server state, client state, and game object positions in debug logs.
    */
    void DebugObjectToServer()
    {
        Vector3 serverStatePos = new Vector3(serverState.posX, serverState.posY, serverState.posZ);
        Vector3 serverStateRot = new Vector3(serverState.rotX, serverState.rotY, serverState.rotZ);
        Vector3 clientStatePos = new Vector3(predictedState.posX, predictedState.posY, predictedState.posZ);
        Vector3 clientStateRot = new Vector3(predictedState.rotX, predictedState.rotY, predictedState.rotZ);
        Debug.Log("Position in server: " + serverStatePos);
        Debug.Log("Rotation in server: " + serverStateRot);
        Debug.Log("Position on client: " + clientStatePos);
        Debug.Log("Rotation on client: " + clientStateRot);
        Debug.Log("Gameobject position : " + transform.position);
        Debug.Log("Gameobject rotation: " + transform.rotation);
    }

    /**
        @authors Darrick Hilburn, Tiffany Fisher

        InitState creates an initial player state.
    */
    void InitState()
    {
        serverState = new PlayerState
        {
            movementNumber = 0,
            posX = -119f,
            posY = 165.08f,
            posZ = -924f,
            rotX = 0,
            rotY = 0,
            rotZ = 0
        };
    }

    /**
        @authors Darrick Hilburn, Tiffany Fisher

        SyncState checks if the rendering should be done on a predicted or client state.
            After determining which state to use, the gameobject position is set to the state position
            and the gameobject rotation is set to the state rotation.
    */
    void SyncState()
    {
        PlayerState renderState = isLocalPlayer ? predictedState : serverState;
        transform.position = new Vector3(renderState.posX, renderState.posY, renderState.posZ);
        transform.rotation = Quaternion.Euler(renderState.rotX, renderState.rotY, renderState.rotZ);
        controller.SetInteger("CharacterState", (int)renderState.animationState);
    }

    /**
        @authors Darrick Hilburn, Tiffany Fisher

        Move checks the player's input and determines which direction to move based on the input and current state.
            Variables representing the change in movement are assigned based on the user input.
            These variables are then applied to the current state to create a new state that is sent out..

        @return Returns a player state that reflects a movement from the current position to a new position.
    */
    PlayerState Move(PlayerState previous, KeyCode newKey)
    {
        float dx = 0, dy = 0, dz = 0;
        float dRotY = 0;

        switch (newKey)
        {
            case (KeyCode.Q):
                dRotY = -rotateSpeed;
                break;
            case (KeyCode.S):
                if (running)
                    dz = -runSpeed;
                else
                    dz = -moveSpeed;
                break;
            case (KeyCode.E):
                dRotY = rotateSpeed;
                break;
            case (KeyCode.W):
                if (running)
                    dz = runSpeed;
                else
                    dz = moveSpeed;
                break;
            case (KeyCode.A):
                if (running)
                    dx = -runSpeed;
                else
                    dx = -moveSpeed;
                break;
            case (KeyCode.D):
                if (running)
                    dx = runSpeed;
                else
                    dx = moveSpeed;
                break;
            case (KeyCode.Space):
                StartCoroutine(Jump(previous));
                break;
            case (KeyCode.LeftShift):
                running = !running;
                break;
        }

       

        return new PlayerState
        {
            movementNumber = 1 + previous.movementNumber,
            posX = dx + previous.posX,
            posY = dy + previous.posY,
            posZ = dz + previous.posZ,
            rotX = previous.rotX,
            rotY = dRotY + previous.rotY,
            rotZ = previous.rotZ,
            animationState = CalcAnimation(dx, dy, dz, dRotY)
        };
    }

    PlayerState RelativeMove(PlayerState previous, KeyCode newKey)
    {
        Vector3 newPos = Vector3.zero;
        float dRotY = 0;

        switch(newKey)
        {
            case (KeyCode.W):
                if (running)
                    newPos = transform.forward * runSpeed;
                else
                    newPos = transform.forward * moveSpeed;
                break;
            case (KeyCode.A):
                if (running)
                    newPos = -transform.right * runSpeed;
                else
                    newPos = -transform.right * moveSpeed;
                break;
            case (KeyCode.S):
                if (running)
                    newPos = -transform.forward * runSpeed;
                else
                    newPos = -transform.forward * moveSpeed;
                break;
            case (KeyCode.D):
                if (running)
                    newPos = transform.right * runSpeed;
                else
                    newPos = transform.right * moveSpeed;
                break;
            case (KeyCode.Q):
                dRotY = -rotateSpeed;
                break;
            case (KeyCode.E):
                dRotY = rotateSpeed;
                break;
            //case (KeyCode.Space):
            //    StartCoroutine(Jump(previous));
            //    break;
            //case (KeyCode.LeftShift):
            //    running = !running;
            //    break;
        }

        return new PlayerState
        {
            movementNumber = 1 + previous.movementNumber,
            posX = newPos.x + previous.posX,
            posY = newPos.y + previous.posY,
            posZ = newPos.z + previous.posZ,
            rotX = previous.rotX,
            rotY = dRotY + previous.rotY,
            rotZ = previous.rotZ,
            animationState = CalcAnimation(newPos.x, newPos.y, newPos.z, dRotY)
        };
    }

    IEnumerator Jump(PlayerState initial)
    {
        jumping = true;
        float step = 0;
        float finalHeight = initial.posY + jumpHeight;

        serverState.posY = initial.posY;
        while(serverState.posY < finalHeight)
        {
            serverState.posY = Mathf.Lerp(serverState.posY, finalHeight, step);
            step += Time.deltaTime;
            UpdatePredictedState();
            yield return new WaitForEndOfFrame();
        }
        serverState.posY = finalHeight;
        UpdatePredictedState();
        step = 0;
        while(serverState.posY > initial.posY)
        {
            serverState.posY = Mathf.Lerp(serverState.posY, initial.posY, step);
            step += Time.deltaTime;
            UpdatePredictedState();
            yield return new WaitForEndOfFrame();
        }
        serverState.posY = initial.posY;
        UpdatePredictedState();
        jumping = false;
    }


    /**
        @authors Darrick Hilburn, Tiffany Fisher

        CalcAnimation determines which animation for the player to play.
        @return Returns an enum representation of the animation to play.
    */
    CharacterState CalcAnimation(float dx, float dy, float dz, float dRotY)
    {
        if(dx.Equals(0) && dy.Equals(0) && dz.Equals(0))
        {
            //Debug.Log("Not moving.");
            return CharacterState.IDLE;
        }
        if (dx != 0 || dz != 0)
        {
            if (dx > 0 || dz > 0)
            {
                if (running)
                {
                    //Debug.Log("Running forward");
                    return CharacterState.RUN_FORWARD;
                }
                else
                {
                    //Debug.Log("Walking forward.");
                    return CharacterState.WALK_FORWARD;
                }
            }
            else
            {
                if (running)
                {
                    //Debug.Log("Running backward");
                    return CharacterState.RUN_BACKWARD;
                }
                else
                {
                    //Debug.Log("Walking backward.");
                    return CharacterState.WALK_BACKWARD;
                }
            }
        }
        Debug.Log("Not moving");
        return CharacterState.IDLE;
    }

    /**
        @authors Darrick Hilburn, Tiffany Fisher

        OnServerStateChanged is the hook for the server state.
            OnServerStateChanged sets the server state to a new state, then checks the pending moves queue.
            If the pending moves queue exists, then the server checks if there are any unprocessed moves.
            If there are, the keys are removed from the queue.
            Regardless of if move processing must occur, the player state is updated on the client.
    */
    void OnServerStateChanged(PlayerState newState)
    {
        serverState = newState;
        if (pendingMoves != null)
        {
            while (pendingMoves.Count > (predictedState.movementNumber - serverState.movementNumber))
            {
                pendingMoves.Dequeue();
            }
            UpdatePredictedState();
        }
    }

    /**
        @authors Darrick Hilburn, Tiffany Fisher

        UpdatePredictedState sets the predicted state to the server state.
            It then checks if there are any pending moves and processes them.
    */
    void UpdatePredictedState()
    {
        predictedState = serverState;
        foreach (KeyCode moveKey in pendingMoves)
        {
            predictedState = RelativeMove(predictedState, moveKey);
        }
    }

    /**
        @authors Darrick Hilburn, Tiffany Fisher

        CmdMoveOnServer tells the server to make a move based on a keypress.
    */
    [Command]
    void CmdMoveOnServer(KeyCode pressedKey)
    {
        serverState = RelativeMove(serverState, pressedKey);
    }
}
#endregion